FROM python:3-alpine AS base
FROM base AS builder
WORKDIR /install
RUN pip install --prefix /install --no-cache-dir aria2p torf

FROM base
ENV ARIA2_HOST=http://localhost
ENV ARIA2_PORT=6800
ENV ARIA2_SECRET=''
ENV RSYNC_SECRET=''
COPY --from=builder /install /usr/local
COPY scripts /scripts
WORKDIR /data
RUN apk add --no-cache bash rsync && chmod a+x -R /scripts
CMD [ "/scripts/run.sh", "/data" ]