#!/bin/bash
THIS=$(dirname ${BASH_SOURCE[0]})
TORRENT_LIST=$(python $THIS/find_not_downloaded.py $1)
while read CMD; do
    if ! [ "$CMD" ]; then
        continue
    fi
    sh -c "$THIS/rsync/$CMD"
done <<<$(python $THIS/aria2_commander.py "$TORRENT_LIST")
